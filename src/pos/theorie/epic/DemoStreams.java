package pos.theorie.epic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Stream;

public class DemoStreams {

    public static void main(String[] args) {

        String input = "123 123 34 656 864 3453 345 345 3645";

        String[] tokens = input.split(" ");

        Stream<String> tokenStream = Arrays.stream(tokens);

        tokenStream.filter(t -> t.length() == 3).forEach(System.out::println);

        OptionalInt oMax = Arrays.stream(tokens).mapToInt(Integer::parseInt).max();
        System.out.println(oMax);

    }
}
