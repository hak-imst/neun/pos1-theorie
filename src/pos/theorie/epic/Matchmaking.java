package pos.theorie.epic;

import java.util.List;

public class Matchmaking {

    public static void main(String[] args) {

        List<String> lines = CodingContest.loadFile("src/pos/theorie/epic/input.txt");

        String line = lines.get(0);

        String[] tokens = line.split(" ");
        int gameCount = Integer.parseInt(tokens[0]);
        int playerCount = Integer.parseInt(tokens[1]);

        int maxScore = Integer.MIN_VALUE;
        int playerId = Integer.MIN_VALUE;

        for (int i = 1; i < lines.size(); i++) {
            line = lines.get(i);

            tokens = line.split(" ");

            int player1Id = Integer.parseInt(tokens[0]);
            int player1Score = Integer.parseInt(tokens[1]);

            int player2Id = Integer.parseInt(tokens[2]);
            int player2Score = Integer.parseInt(tokens[3]);

            if (player1Score > player2Score) {

                if (player1Score > maxScore) {
                    maxScore = player1Score;
                    playerId = player1Id;
                }

            } else if (player1Score < player2Score) {

                if (player2Score > maxScore) {
                    maxScore = player2Score;
                    playerId = player2Id;
                }

            } else {

                if (player1Score > maxScore) {
                    maxScore = player1Score;
                }

                // lowest player id
                if (player1Id < player2Id) {
                    playerId = player1Id;
                } else {
                    playerId = player2Id;
                }
            }

        }


        System.out.println(playerId + " " + maxScore);




    }
}
