package pos.theorie.epic;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class QuantumLevel2 {


    public static void main(String[] args) {

        List<String> lines = CodingContest.loadFile("level2/level2_5.in");

        HashMap<String, Integer> flights = new HashMap<>();

        // 34103,51.331,-0.393,3962.0,LPPT,EGLL,26949
        for (int i = 1; i < lines.size(); i++) {
            String line = lines.get(i);

            String[] tokens = line.split(",");
            String start = tokens[4];
            String dest = tokens[5];
            String key = start + " " + dest;

            int count = 1;

            if (flights.containsKey(key)) {
                count += flights.get(key);
            }

            flights.put(key, count);
        }

        flights.keySet()
                .stream()
                .sorted()
                .forEach(key -> System.out.println(key + " " + flights.get(key)));

    }

}
