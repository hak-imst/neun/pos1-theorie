package pos.theorie.epic;

import java.io.*;
import java.util.List;

public class SmartGrid {
    public static void main(String[] args) {

        List<String> lines = CodingContest.loadFile("src/pos/theorie/epic/input.txt");

        int min = Integer.MAX_VALUE;
        int minPriceId = 0;

        //int j = lines.stream().mapToInt(s -> Integer.parseInt(s)).reduce(Integer::min).getAsInt();

        for (int i = 1; i < lines.size(); i++) {
            String line = lines.get(i);
            int price = Integer.parseInt(line);

            if (price < min) {
                min = price;
                minPriceId = i - 1;
            }
        }

        System.out.println(minPriceId);
    }
}
