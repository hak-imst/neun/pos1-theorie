package pos.theorie.projekte;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws ParseException {

        Scanner scanner = new Scanner(System.in);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date date1 = sdf.parse("09.06.2022 11:07:13");

        Messwert m1 = new Messwert(date1, 22);
        Messwert m2 = new Messwert(sdf.parse("09.06.2022 11:07:14"), 23);
        Messwert m3 = new Messwert(sdf.parse("09.06.2022 11:07:15"), 24);

        List<Messwert> messwerte = new ArrayList<>();
        messwerte.add(m1);
        messwerte.add(m2);
        messwerte.add(m3);

        System.out.println("Bitte Messwert eingeben [-20 bis 50]:");
        double wert = Double.parseDouble(scanner.nextLine());
        Date jetzt = new Date();

        messwerte.add(new Messwert(jetzt, wert));

        for (Messwert m : messwerte) {
            System.out.println(m);
        }

        // https://gitlab.com/hak-imst/neun/pos1-theorie
    }
}
