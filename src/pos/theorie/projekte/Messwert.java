package pos.theorie.projekte;

import java.util.Date;

public class Messwert {

    private Date zeitpunkt;
    private double wert;

    public Messwert(Date zeitpunkt, double wert) {
        this.zeitpunkt = zeitpunkt;
        this.wert = wert;
    }

    @Override
    public String toString() {
        return "Messwert{" +
                "zeitpunkt=" + zeitpunkt +
                ", wert=" + wert +
                '}';
    }
}
