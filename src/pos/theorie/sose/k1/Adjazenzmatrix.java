package pos.theorie.sose.k1;

public class Adjazenzmatrix {

    public static void main(String[] args) {

        // zeilen x spalten
        int[][] adjazenzmatrix = new int[6][12];

        adjazenzmatrix[0][0] = 1; // kante zw knoten 1 und 1
        adjazenzmatrix[0][1] = 1; // kante zw knoten 1 und 2
        adjazenzmatrix[0][4] = 1; // kante zw knoten 1 und 5

        adjazenzmatrix[1][0] = 1; // kante zw knoten 2 und 1
        adjazenzmatrix[1][2] = 1; // kante zw knoten 2 und 3
        adjazenzmatrix[1][4] = 1; // kante zw knoten 2 und 5

        adjazenzmatrix[5][11] = 8; // kante zw knoten 2 und 5

        for (int zeile = 0; zeile < adjazenzmatrix.length; zeile++) {

            for (int spalte = 0; spalte < adjazenzmatrix[zeile].length; spalte++) {
                System.out.print(adjazenzmatrix[zeile][spalte] + " ");
            }
            System.out.println();

        }


    }
}
