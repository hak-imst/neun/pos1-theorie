package pos.theorie.sose.k2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Datenstrukturen {

    public static final char WASSER = 'W';
    public static final int ARRAY_LENGTH = 10;

    public static void main(String[] args) {

        int[] zahlen = new int[ARRAY_LENGTH];

        zahlen[9] = 4;
        System.out.println(zahlen[3]);

        for (int z : zahlen) {
            System.out.println(z);
        }

        for (int i = 0; i < zahlen.length; i++) {
            //zahlen[i] = 99;
            int z = zahlen[i];
            System.out.println(z);
        }

        String[] texte = {"A", "B", "C"};


        // mehrdimensionale Array
        double[][] tabelle = new double[4][10];
        tabelle[1][8] = 4.0;

        for (int z = 0; z < tabelle.length; z++) {
            for (int s = 0; s < tabelle[z].length; s++) {
                tabelle[z][s] = 1;
                System.out.println(tabelle[z][s]);
            }
        }



        List<Integer> zahlen2 = new ArrayList<>();
        zahlen2.add(4);
        zahlen2.add(6);
        zahlen2.add(2);
        zahlen2.add(1,20);
        zahlen2.set(0, 5);

        for (Integer z : zahlen2) {
            System.out.println(z);
        }

        char[][] spielfeld = new char[10][5];
        spielfeld[0][0] = WASSER;


        // array + schleife
        long[] grosseZahlen = new long[100];
        grosseZahlen[0] = 5;

        for(int i = 1; i < grosseZahlen.length; i++) {
            grosseZahlen[i] = grosseZahlen[ i - 1 ] * 2;
        }

        for (long z : grosseZahlen) {
            System.out.print(z + " ");
        }

    }

}
