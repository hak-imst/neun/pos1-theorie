package pos.theorie.sose.k2;

import java.util.Objects;

public class Obst {

    private String name;
    private boolean essbar;

    public Obst(String name, boolean essbar) {
        this.name = name;
        this.essbar = essbar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEssbar() {
        return essbar;
    }

    public void setEssbar(boolean essbar) {
        this.essbar = essbar;
    }

    @Override
    public boolean equals(Object o) {
        return true;
        /*if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Obst obst = (Obst) o;
        return essbar == obst.essbar && Objects.equals(name, obst.name);
   */ }

    @Override
    public int hashCode() {
        return Objects.hash(name, essbar);
        //return 1;
    }

    @Override
    public String toString() {
        return "Obst{" +
                "name='" + name + '\'' +
                ", essbar=" + essbar +
                '}';
    }
}
