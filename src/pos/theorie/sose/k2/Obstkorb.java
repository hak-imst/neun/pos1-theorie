package pos.theorie.sose.k2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Obstkorb {

    public static void main(String[] args) {

        Set<Obst> inhalt = new HashSet<>();

        Obst apfel = new Obst("Apfel", true);
        Obst birne = new Obst("Birne", false);
        Obst apfel2 = new Obst("apfel", true);

        System.out.println(apfel.hashCode());
        System.out.println(apfel2.hashCode());
        System.out.println(birne.hashCode());

        inhalt.add(apfel);
        //inhalt.add(apfel2);
        inhalt.add(birne);

        System.out.println(inhalt.contains(apfel2));


        HashMap<Obst, Obst> map = new HashMap<>();
        map.put(apfel, apfel);
        map.put(birne, birne);

        System.out.println(map.get("AAAA"));
        System.out.println(map.get(apfel));



    }

}
