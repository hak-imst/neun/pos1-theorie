package pos.theorie.sose.k4;

import pos.theorie.sose.k6.MyNode;

public class C {

    public static void main(String[] args) {
        System.out.println(dekrement1(10));
        System.out.println(dekrement2(10));

        int x = 4;

        System.out.println(5 * --x * -1);
    }

    static int dekrement1(int x) {
        return x--;
    }

    static int dekrement2(int x) {
        return --x;
    }

}
