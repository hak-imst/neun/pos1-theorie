package pos.theorie.sose.k6;

public interface Berechnung {
    public static final int WERT = 4;

    double berechne();

    Bruch addieren(Bruch b);
    
}
