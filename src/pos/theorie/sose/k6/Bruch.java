package pos.theorie.sose.k6;

import java.util.ArrayList;
import java.util.List;

public class Bruch implements Berechnung, Grundrechenarten {
    private int zaehler;
    private int nenner = 1;

    public static final Bruch NULL = new Bruch(0,1);
    public static final Bruch EINS = new Bruch(1,1);
    public static final Bruch HALBE = new Bruch(1,2);

    private static int anzahlBrueche = 0;

    private static void zaehleBruche() {
        anzahlBrueche++;
    }

    public Bruch() {
    }

    public Bruch(int z) {
        this(z, 1);
    }

    public Bruch(int z, int n) {
        this.zaehler = z;
        this.nenner = n;
    }

    public int getNenner() {
        return nenner;
    }

    public void setNenner(int nenner) {
        if (nenner >= 0) {
            this.nenner = nenner;
        } else {
            System.out.println("Fehler");
        }
    }

    @Override
    public Bruch addieren(Bruch b) {
        // 2/4 + 2/3
        Bruch ergebnis = new Bruch();
        ergebnis.nenner = this.nenner * b.nenner;
        ergebnis.zaehler = this.zaehler * b.nenner + b.zaehler * this.nenner;
        return ergebnis;
    }

    @Override
    public String toString() {
        return "Bruch{" +
                "zaehler=" + zaehler +
                ", nenner=" + nenner +
                '}';
    }

    @Override
    public double berechne() {
        return 0;
    }

    public static void main(String[] args) {
        Bruch a = new Bruch(2,4);
        Bruch b = new Bruch(2,3);
        Bruch c = new Bruch(1, 2);

        Bruch e  = a.addieren(b);
        System.out.println(e);

        b.addieren(c);

        double d = Double.parseDouble("2.3");

        Bruch.anzahlBrueche = 0;
        Bruch.zaehleBruche();

        b.zaehler = 5;

        List<Integer> list = new ArrayList<Integer>();

    }

}
