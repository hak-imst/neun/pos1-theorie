package pos.theorie.sose.k6;

import java.util.Objects;

public class Fahrzeug {

    private int leistung;

    public Fahrzeug(int leistung) {
        this.leistung = leistung;
    }

    public void fahren() {
        System.out.println("allgemeines fahren");
    }
}
