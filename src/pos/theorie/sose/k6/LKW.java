package pos.theorie.sose.k6;

public class LKW extends Fahrzeug {

    private double ladeVolumen;

    public LKW(int leistung) {
        super(leistung);
    }

    @Override
    public void fahren() {
        System.out.println("lkw langsames fahren");
        super.fahren();
    }
}
