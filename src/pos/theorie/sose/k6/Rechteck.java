package pos.theorie.sose.k6;

public class Rechteck extends Viereck {

    @Override
    public void berechneFlaeche() {
        System.out.println("berechneFlaeche Rechteck");
    }

    public void berechneNurRechteck() {

    }

    public static void main(String[] args) {

        double x = 87.0;
        int y = (int) 46L;

        Viereck v1 = new Rechteck();
        v1.berechneFlaeche();
        ((Rechteck)v1).berechneNurRechteck();

        v1 = new Viereck();












        Viereck v = new Viereck();
        v.berechneFlaeche();

        Rechteck r = new Rechteck();
        r.berechneFlaeche();
        r.berechneNurRechteck();        // klappt, da ein Rechteck-Objekt vorliegt (dynamischer Typ)

        Viereck v2 = new Rechteck();    // implizite Typumwandlung, stat. Typ Viereck, dyn. Typ Rechteck
        v2.berechneFlaeche();
        //v2.berechneNurRechteck();     // klappt nicht, da der statische Typ Viereck ist die Methode dort nicht existiert
        ((Rechteck)v2).berechneNurRechteck();    // expliziter cast auf Typ Rechteck

        v2 = new Viereck();     // dynamischer Typ kann sich ändern

        //Rechteck r2 = (Rechteck) new Viereck();   nicht möglich: ClassCastException

        //Rechteck r2 = (Rechteck) new Viereck(); // explizte Typumwandlung

        double d = 3;       // implizite Typumwandlung
        int i = (int) 4.5;  // explizite Typumwandlung

    }
}
