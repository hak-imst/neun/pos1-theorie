package pos.theorie.sose.k7;

import java.util.Comparator;

/**
 * beschreibung der klasse
 * @author Dominik
 * @version 1.0
 */
public class Auto implements Comparator<Auto> {

    /**
     * leistung in ps (0-x)
     */
    private final int leistung;

    public Auto(int leistung) {
        this.leistung = leistung;
    }

    /**
     * vergleicht zwei autos anhand der leistung
     * @param o1
     * @param o2
     * @return
     */
    @Override
    public int compare(Auto o1, Auto o2) {
        // Differenz der Leistungen als Grundlage für die Sortierung
        return o1.leistung - o2.leistung;
    }

}
