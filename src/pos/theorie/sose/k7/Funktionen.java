package pos.theorie.sose.k7;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class Funktionen {


    public static void main(String[] args) {


        int i1 = 4;
        int i2 = 5;

        if (i1 < i2) {

        }

        char c1 = 'G';
        char c2 = 'J';

        if (c1 < c2) {

        }

        String s1 = "EBC";
        String s2 = "D";

        // geht nicht für Referenztypen
      //  if (s1 > s2) {
      //  }

        System.out.println(s1.compareTo(s2));


        List<Integer> zahlen = Arrays.asList(4, 5, 6, 7, 5, 6, 4, 7);

        // aufsteigend sortieren
        Collections.sort(zahlen);

        // absteigend sortieren mit eigener Comparator-Klasse
        Collections.sort(zahlen, new IntegerAbsteigendComparator());

        // absteigend sortieren mit anonymer Comparator-Klasse
        Collections.sort(zahlen, new Comparator<>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });

        // absteigend sortieren mit Lamda-Ausdruck
        Collections.sort(zahlen, (o1, o2) -> o2 - o1 );


        // Funktion x + 1.0
        Function<Integer, Double> add1 = x -> x + 1.0;
        double d = add1.apply(6);


        // Funktion doppelte Länge
        Function<String, Integer> doubleLength = s -> s.length() * 2;
        int l = doubleLength.apply("Hallo Lambda");


        List<String> namen = new ArrayList<>();
        namen.add("Schrödinger");
        namen.add("Bissingen");
        namen.add("Bossingen");
        namen.add("Bussingen");
        namen.add("Schickelmickel");

        // Filtern aller String mit ss
        namen.stream().filter(n -> n.contains("ss")).forEach(name -> System.out.println(name));

        // Filter aller String mit ss, mit Methodenreferenz
        namen.stream().filter(n -> n.contains("ss")).forEach(System.out::println);

        // Map aller Werte
        zahlen = Arrays.asList(4, 9, 16, 25, 36, 49);
        Stream<Double> wurzeln = zahlen.stream().map(zahl -> Math.sqrt(zahl));
        wurzeln.forEach(System.out::println);
        System.out.println(zahlen);     // Orignalwerte bleiben unverändert

        // Reduce (hat immer zwei Parameter und ein Ergebnis)
        Optional<Integer> opt = zahlen.stream().reduce((a,b) -> a + b);
        //Optional<Integer> opt = zahlen.stream().reduce(Integer::sum);

        System.out.println(opt);
        System.out.println(opt.isPresent());
        System.out.println(opt.get());

        Integer maximum = zahlen.stream().reduce(Integer::max).get();

    }

}
