package pos.theorie.sose.k7;

import java.util.*;
import java.util.stream.Stream;

public class Person {

    private String name;
    private int alter;
    private Geschlecht geschlecht;

    public Person(String name, int alter, Geschlecht geschlecht) {
        this.name = name;
        this.alter = alter;
        this.geschlecht = geschlecht;
    }

    public int getAlter() {
        return alter;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", alter=" + alter +
                ", geschlecht=" + geschlecht +
                '}';
    }

    public static void main(String[] args) {
        List<Person> personen = new ArrayList<>();
        personen.add(new Person("A", 2, Geschlecht.X));
        personen.add(new Person("B", 32, Geschlecht.M));
        personen.add(new Person("C", 22, Geschlecht.M));
        personen.add(new Person("D", 12, Geschlecht.W));

        for(Person p : personen) {
            System.out.println(p);
        }

        personen.forEach(person -> System.out.println(person));

        personen.stream().forEach(System.out::println);

        //Collections.sort(personen, new PersonAlterAbsteigendComparator());

        personen.stream()
                .sorted(new PersonAlterAbsteigendComparator())
                .forEach(System.out::println);

        personen.stream()
                .sorted((o1, o2) -> o2.getAlter() - o1.getAlter())
                .forEach(System.out::println);

        double durschnitt = personen.stream()
                .filter(p -> p.geschlecht == Geschlecht.M)
                .mapToDouble(p -> p.alter)
                .average().getAsDouble();

        System.out.println(durschnitt);

    }
}
