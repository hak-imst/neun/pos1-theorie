package pos.theorie.wise.k1;

public class Demo {

    public static void main(String[] args) {
        boolean a = false, b = true, c = false;

        if ( !(a || b) && !!c) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }

}
