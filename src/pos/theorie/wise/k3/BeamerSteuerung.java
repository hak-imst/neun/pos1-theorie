package pos.theorie.wise.k3;

import java.util.Scanner;

public class BeamerSteuerung {


    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);

        System.out.println("Bitte Quelle wählen (1=PC, 2=HDMI):");

        int wahl = scan.nextInt();

        if (wahl == 1 || wahl == 2) {
            System.out.println("Quelle " + wahl + " gewählt");

            if (wahl == 1) {
                System.out.println("Quelle PC gewählt");
            }

            if (wahl == 2) {
                System.out.println("Quelle HDMI gewählt");
            }

        } else {
            System.out.println("Ungültige Quelle");
        }

        // alternativ
        /*
        if (wahl == 1) {
            System.out.println("Quelle PC gewählt");
        } else if (wahl == 2) {
            System.out.println("Quelle HDMI gewählt");
        } else {
            System.out.println("Ungültige Quelle");
        }*/


        System.out.println("Bitte eine Zahl zwischen 10 und 30 eingeben:");
        double x = scan.nextDouble();

        if (x >= 10 && x <= 30) {
            System.out.println("Zahl gültig");
        } else {
            System.out.println("Ungültige Zahl");
            System.exit(0);
        }

        if (x >= 10) {
            if (x <= 30) {
                System.out.println("Zahl gültig");
            } else {
                System.out.println("Zahl zu groß");
                System.exit(0);
            }
        } else {
            System.out.println("Zahl zu klein");
            System.exit(0);
        }

        x *= 2; // kurzschreibweise
        //x = x * 2;

        System.out.println("Ergebnis x*2=" + x);

    }

}
