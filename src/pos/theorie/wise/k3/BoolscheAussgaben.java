package pos.theorie.wise.k3;

import java.util.Scanner;

public class BoolscheAussgaben {

    public static void main(String[] args) {

        boolean aussgabe1 = false;
        boolean aussgabe2 = true;

        boolean ergebnis = aussgabe1 && aussgabe2;
        System.out.println(ergebnis);

        System.out.println(!aussgabe1);

        System.out.println(aussgabe1 ^ aussgabe2);

        Scanner scan = new Scanner(System.in);

        System.out.println("bitte note eingeben [1-5]:");

        int note = Integer.parseInt(scan.next());   // 1-5

        /*if (note < 1 || note > 5) {
            // ungültig
        } else {
            // gültig
            System.out.println("note ungültig [1-5]");
        }*/

        if (note >= 1 && note <= 5) {
            // gültig
        } else {
            // ungültig
            System.out.println("note ungültig [1-5]");
            note = 3;
        }


        int anwesenheit = 80;   // 0-100 %

        boolean bestanden = (note != 5) && (anwesenheit >= 75);
        //boolean bestanden = (note == 1 || note  == 2 || note == 3 || note == 4) && (anwesenheit >= 75);

        System.out.println("bestanden: " + bestanden);


    }

}
