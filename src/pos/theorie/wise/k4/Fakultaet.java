package pos.theorie.wise.k4;

import java.util.Scanner;

public class Fakultaet {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n, fakultaet = 1;

        System.out.println("Bitte Zahl n eingeben:");
        n = Integer.parseInt(scan.nextLine());

        while(n > 1) {
            fakultaet = fakultaet * n;
            n--;
        }

        System.out.println("Fakultät: " + fakultaet);

    }
}
