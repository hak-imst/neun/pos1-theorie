package pos.theorie.wise.k4;

import javax.swing.*;
import java.util.Scanner;

public class WeitereBerechnung {

    public static void main(String[] args) {

        // Weitere Berechnung durchführen - Variante 1 String
        Scanner scan = new Scanner(System.in);

        String weitereBerechnungString;

        do {
            System.out.println("Weitere Berechnung? ja/nein");
            weitereBerechnungString = scan.nextLine();

        } while (weitereBerechnungString.equals("ja"));


        // Weitere Berechnung durchführen - Variante 2 charAt

        do {
            System.out.println("Weitere Berechnung? ja/nein");
            weitereBerechnungString = scan.nextLine();

        } while (weitereBerechnungString.charAt(0) == 'j');


        // Weitere Berechnung durchführen - Variante 3 boolean
        boolean weitereBerechnungBoolean;

        do {
            System.out.println("Weitere Berechnung? true/false");
            weitereBerechnungBoolean = scan.nextBoolean();

        } while (weitereBerechnungBoolean);


        // Weitere Berechnung durchführen - Variante 4 JOptionPane
        int auswahl = JOptionPane.showConfirmDialog(null, "Weitere Berechnung? ja/nein");

    }
}
