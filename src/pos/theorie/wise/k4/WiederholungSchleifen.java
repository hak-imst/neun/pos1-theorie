package pos.theorie.wise.k4;

import java.util.Scanner;

public class WiederholungSchleifen {

    public static void main(String[] args) {

        // 05.11.2020 =========================================

        // Eingabe von 3 Zahlen, aufsummieren
        Scanner sc = new Scanner(System.in);

        System.out.println("Bitte geben Sie drei Ganzzahlen ein:");
        int zahl1 = sc.nextInt();
        int zahl2 = sc.nextInt();
        int zahl3 = sc.nextInt();

        int summe = zahl1;
        summe = summe + zahl2;
        summe += zahl3;  // Kurzschreibweise

        System.out.println("Summe: " + summe);


        // Eingabe von X Zahlen, aufsummieren

        System.out.println("Wieviele Zahlen wollen Sie eingeben:");
        int anzahl = sc.nextInt();

        System.out.println("Bitte geben Sie " + anzahl + " Ganzzahlen ein:");

        summe = 0;
        for(int i = 0; i < anzahl; i++) {
            int zahl = sc.nextInt();
            summe = summe + zahl;
        }

        System.out.println("Summe: " + summe);

        sc.close();
    }
}
