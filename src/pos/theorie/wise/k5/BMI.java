package pos.theorie.wise.k5;

import java.util.Scanner;

public class BMI {

    public static void main(String[] args) {

        // eingabe
        Scanner scanner = new Scanner(System.in);

        System.out.println("Bitte Größe in cm eingeben:");
        int cm = Integer.parseInt(scanner.nextLine());

        System.out.println("Bitte Gewicht in kg eingeben:");
        int kg = Integer.parseInt(scanner.nextLine());

        // berechnung
        double bmi = berechnung(cm, kg);

        // ausgabe
        ausgabe(bmi);
    }


    public static double berechnung(int cm, int kg) {
        return kg / (cm/100.0 * cm/100.0);
    }

    public static void ausgabe(double bmi) {
        System.out.println("Ihr BMI ist: " + bmi);

        if (bmi < 19) {
            System.out.println("Untergewicht");
        } else if (bmi >= 19 && bmi <= 25) {
            System.out.println("Normalgewicht");
        } else if (bmi > 25 && bmi < 30) {
            System.out.println("Übergewicht");
        } else {
            System.out.println("Adipositas");
        }
    }

}
