package pos.theorie.wise.k5;

import java.math.BigInteger;

public class BigIntegerDemo {

    public static void main(String[] args) {

        BigInteger a = new BigInteger("5");
        BigInteger b = BigInteger.ONE; // new BigInteger("1")
        BigInteger c = a.add(b);

        System.out.println(c);

        if (a.compareTo(BigInteger.TEN) == 0) {
            // a == 10
        }
        if (a.compareTo(BigInteger.TEN) < 1) {
            // a < 10
        }
        if (a.compareTo(BigInteger.TEN) > 1) {
            // a > 10
        }





    }


}
