package pos.theorie.wise.k5;

public class Fakultaet {

    public static void main(String[] args) {

        System.out.println(fakultaet(5));

    }

    static int fakultaet(int n) {
        int f;
        if (n < 0) {
            f = 0;
        } else {
            if (n == 0) {
                f = 1;
            } else {
                f = n * fakultaet(n - 1);
            }
        }
        return f;
    }


}
