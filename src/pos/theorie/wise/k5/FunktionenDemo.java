package pos.theorie.wise.k5;

import java.util.Scanner;

public class FunktionenDemo {

    /**
     * beschreibung der methode...
     *
     * @param x der zu verdoppeltende wert
     * @return gibt das doppelte ergebnis zurück
     */
    public static double verdopple(double x) {
        double ergebnis;
        ergebnis = x * 2;

        x = 0;
        return ergebnis;
    }

    /**
     * verdoppelt einen wert
     *
     * @param x
     * @param y
     * @return
     */
    public static double verdopple(int x, double y) {
        return x * 2.0;
    }

    /**
     * anzeigen...
     *
     * @param x
     */
    public static void verdoppleUndAnzeigen(double x) {

    }

    public static void main(String[] args) {

        System.out.println(333.0);

        Scanner scan = new Scanner(System.in);
        int i = 40;

        double e = verdopple(444);
        System.out.println(e);

        System.out.println(verdopple(4));

        double f = verdopple(i);

        System.out.println(i);


    }
}
