package pos.theorie.wise.k5;

public class Punkt {

    public int x, y;

    public Punkt() {
    }

    public Punkt(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static void main(String[] args) {

        Punkt a = new Punkt();
        Punkt b = new Punkt(8,9);

        a.setLocation(5, 6);
        b.setLocation(3,4);

        a.x = 10;
        System.out.println(b.x); //
        b.y = 5;
        System.out.println( a.y ); // ?


    }


}
