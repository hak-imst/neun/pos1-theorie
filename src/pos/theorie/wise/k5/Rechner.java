package pos.theorie.wise.k5;

public class Rechner {

    /**
     * Addition unter Verwendung von CallByReference
     * Box1 und Box2 werden nicht kopiert, sondern als Referenz auf das Objekt (die Speicheradresse) übergeben
     *
     * @param box1
     * @param box2
     * @return
     */
    public int addition(Box box1, Box box2) {
        int ergebnis = box1.value + box2.value;

        // ändert den Wert von Value in Box1 (auch außerhalb der Methode)
        box1.value = 0;

        return ergebnis;
    }

    /**
     * Addition unter Verwendung von CallByValue
     * a und b werden als Kopien übergeben
     *
     * @param a erste Zahl
     * @param b zweite Zahl
     * @return a + b
     */
    public int addition(int a, int b) {
        int ergebnis = a + b;

        // ändert nur den Wert des Parameters a (nicht außerhalb der Methode)
        a = 0;

        return ergebnis;
    }

    /**
     * Addition zweier Zahlen
     *
     * @param a erste Zahl
     * @param b zweite Zahl
     * @return ergebnis, a + b
     */
    public int addition(double a, int b) {
        return (int) (a + b);
    }

    /**
     * Addition und Ausgabe zweier Zahlen
     *
     * @param a erste Zahl
     * @param b zweite Zahl
     */
    public void additionUndAusgabe(int a, int b) {
        int ergebnis = a + b;
        System.out.println(ergebnis);
    }

}
