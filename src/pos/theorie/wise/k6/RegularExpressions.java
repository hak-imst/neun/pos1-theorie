package pos.theorie.wise.k6;

public class RegularExpressions {

    public static void main(String[] args) {



        String name = "NEUN";

        System.out.println(name.matches("\\d@hakimst.rocks"));

















        String email = "do.neuner@tsn.at";

        if(email.matches("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")){
            System.out.println("email ist gültig");
        } else {
            System.out.println("email ist nicht gültig");
        }

        if(email.matches("\\w+@\\w+\\.[a-zA-Z]{2,3}")){
            System.out.println("email ist gültig");
        } else {
            System.out.println("email ist nicht gültig");
        }


        String[] tokens = email.split("\\..");

        // foreach loop
        for (String t : tokens) {
            System.out.println(t);
        }

        // for loop
        /*for (int i = 0; i < tokens.length; i++) {
            System.out.println(tokens[i]);
        }*/



        // a(), a(ba), a(ba)(ba), a(ba)(ba)(ba), ...

    }

}
